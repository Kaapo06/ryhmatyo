<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Css/style.css">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <title>Kirjautuminen</title>
</head>
<body class="tausta">
    


<div class="wrapper">
    <div class="boksi">
    <form action="login.php" method="post" >
        <h1>Sisään Kirjautuminen</h1>

        <?php if(isset($_GET['error'])) { ?>
    <p class="error"> <?php echo $_GET['error'];  ?> </p>
    <?php } ?>

        <div class="input-box">
            <input type="text" id="kayttaja" name="uname" placeholder="Käyttäjänimi" required>
        <i class='bx bxs-user'></i>
        </div>

        <div class="input-box">
            <input class="salasana" name="password" type="password" placeholder="Salasana" required>
            <i class='bx bxs-lock-alt' ></i>
        </div>
        
        <div class="remember-forgot">
            <label><input type="checkbox">Muista minut
            </label>
            <a href="#">Unohditko salasanan?</a>
        </div>



        <button type="submit" value="submit" class="buttoni">Kirjaudu sisään</button>
        <br><br>
        <div class="register-link">
            <p>Ei ole käyttäjää? <a href="luo_kayttaja.php">Luo käyttäjä</a></p>
        </div>
    </div>
        </form>
    </div>
    <footer>
        <p>Blogisivu</p>
    </footer>

    <script src="./Javascript/script.js"></script>
</body>
</html>