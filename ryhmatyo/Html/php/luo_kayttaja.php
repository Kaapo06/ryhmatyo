<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Css/style.css">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <title>Luo käyttäjä</title>
</head>
<body class="tausta">
 

 
<div class="wrapper">
    <div class="boksi">
        <?php
        // Establish a connection to the database
        try {
            $yhteys = new PDO("mysql:host=localhost;dbname=test_db", "root", "");
            $yhteys->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("VIRHE: " . $e->getMessage());
        }
 
        // Check if the form is submitted
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Get values from the registration form
            $uname = $_POST['uname'];
            $password = $_POST['password']; // Store plaintext password
 
            // Insert user into 'users' table
            $stmt = $yhteys->prepare("INSERT INTO users (user_name, password) VALUES (:user_name, :password)");
 
            // Bind parameters
            $stmt->bindParam(':user_name', $uname);
            $stmt->bindParam(':password', $password);
 
            // Execute the statement
            $stmt->execute();
 
            // Redirect the user to a success page
            header('Location: profiili.php');
            exit();
        }
        ?>
 
        <form action="" method="post">
            <h1>Luo käyttäjä</h1>
 
            <div class="input-box">
                <input type="text" name="uname" placeholder="Käyttäjänimi" required>
                <i class='bx bxs-user'></i>
            </div>
 
            <div class="input-box">
                <input type="password" name="password" placeholder="Salasana" required>
                <i class='bx bxs-lock-alt'></i>
            </div>
 
            <button type="submit" class="buttoni">Luo käyttäjä</button>
 
            <br><br>
            <div class="register-link">
                <p>On jo käyttäjä? <a href="profiili.php">Kirjaudu sisään</a></p>
            </div>
        </form>
    </div>
</div>
 
<footer>
    <p>Blogisivu</p>
</footer>
</body>
</html>
 