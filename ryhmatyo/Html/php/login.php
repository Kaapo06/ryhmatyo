<?php
session_start();
// yhdistetään tietokantaan
include "db_conn.php";

// Tarkistetaan, onko käyttäjänimi (uname) ja salasana (password) lähetetty lomakkeelta
if(isset($_POST['uname']) && isset($_POST['password'])) {

    function validate ($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    // haetaan käyttäjän antamat tiedot
    $uname = validate($_POST['uname']);
    $pass = validate($_POST['password']);

    $sql = "SELECT * FROM users WHERE user_name= '$uname' AND password='$pass'";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) === 1) {
        $row = mysqli_fetch_assoc($result);
        // katsotaan onko annettu salasana ja nimi samat kuin tietokannassa
        if($row['user_name'] === $uname && $row['password'] === $pass) {
            echo "Logged In!";

            // Tallennetaan käyttäjätiedot istuntoon
            $_SESSION['user_name'] = $row['user_name'];
            $_SESSION['name'] = $row['name'];
            $_SESSION['id'] = $row['id'];
            // Ohjataan käyttäjä profiilisivulle
            header("Location: profiili.php");
            exit();
        } else {        //Laittaa error messagen jos tunnukset ovat väärät
            header("Location: kirjautuminen.php?error=Incorrect User name or password"); 
            exit();
        }
    } else {
        header("Location: kirjautuminen.php?error=Incorrect User name or password");
        exit();
    }

} 

else {
    header("Location: kirjautuminen.php");
    exit();
}
?>