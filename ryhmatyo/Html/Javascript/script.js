document.addEventListener('DOMContentLoaded', function () {
    const blogPosts = document.getElementById('blog-posts');
    const newPostLink = document.getElementById('new-post-link');
    const newPostForm = document.getElementById('new-post-form');
    const publishButton = document.getElementById('publish-button');
    const postCounter = document.getElementById('post-counter');

    // Example posts (you can replace this with loading posts from a server)
    let posts = [
        { id: 1, title: 'Ensimmäinen blogi', content: 'Tässä ensimmäinen blogi.', comments: [], timestamp: new Date() },
        { id: 2, title: 'Toinen blogi', content: 'Tässä on hieno toinen blogi.', comments: [], timestamp: new Date() }
    ];

    // Display existing posts
    displayPosts();

    function displayPosts() {
        blogPosts.innerHTML = '';

        posts.forEach(post => {
            const li = document.createElement('li');
            li.innerHTML = `
                <h2>${post.title}</h2>
                <p>${post.content}</p>
                <ul class="comments" id="comments-${post.id}"></ul>
                <textarea id="comment-${post.id}" class="comment-textarea" placeholder="Lisää kommentti"></textarea>
                <button class="add-comment-button" data-id="${post.id}">Lisää kommentti</button>
                <br>
                <button class="edit-button" data-id="${post.id}">Muokkaa Blogia</button>
                <br>
                <button class="delete-button" data-id="${post.id}">Poista Blogi</button>
                <br>
                <br>
                <p><strong>Julkaistu:</strong> ${formatTimestamp(post.timestamp)}</p>`;
            blogPosts.appendChild(li);

            // Display comments
            const commentsList = document.getElementById(`comments-${post.id}`);
            post.comments.forEach(comment => {
                const commentLi = document.createElement('li');
                commentLi.textContent = comment;
                commentsList.appendChild(commentLi);
            });
        });

        updatePostCounter(); // Update the post counter

        // Add event listeners to delete buttons, add comment buttons, and edit buttons
        const deleteButtons = document.querySelectorAll('.delete-button');
        deleteButtons.forEach(button => {
            button.addEventListener('click', function () {
                const postId = parseInt(this.dataset.id);
                deletePost(postId);
            });
        });

        const addCommentButtons = document.querySelectorAll('.add-comment-button');
        addCommentButtons.forEach(button => {
            button.addEventListener('click', function () {
                const postId = parseInt(this.dataset.id);
                addComment(postId);
            });
        });

        const editButtons = document.querySelectorAll('.edit-button');
        editButtons.forEach(button => {
            button.addEventListener('click', function () {
                const postId = parseInt(this.dataset.id);
                editPost(postId);
            });
        });
        
        // Edit post function
        function editPost(postId) {
            const post = posts.find(post => post.id === postId);
        
            // Display the edit form
            const editForm = document.getElementById('edit-form');
            editForm.style.display = 'block';
        
            // Populate form fields with existing post content
            const editTitleInput = document.getElementById('edit-title');
            const editContentTextarea = document.getElementById('edit-content');
            editTitleInput.value = post.title;
            editContentTextarea.value = post.content;
        
            // Event listener for update button
            const updateButton = document.getElementById('update-button');
            updateButton.addEventListener('click', function () {
                // Update the post with the new content
                post.title = editTitleInput.value;
                post.content = editContentTextarea.value;
        
                // Hide the edit form
                editForm.style.display = 'none';
        
                // Display the updated posts
                displayPosts();
            });
        }
    }

    function formatTimestamp(timestamp) {
        const options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
        return new Intl.DateTimeFormat('fi-FI', options).format(timestamp);
    }

    function deletePost(postId) {
        // Remove the post with the given ID
        posts = posts.filter(post => post.id !== postId);

        // Display the updated posts
        displayPosts();
    }

    function addComment(postId) {
        const commentInput = document.getElementById(`comment-${postId}`);
        const comment = commentInput.value;

        if (comment.trim() === '') {
            alert('Kommentti ei voi olla tyhjä.');
            return;
        }

        // Add the comment to the post
        const post = posts.find(post => post.id === postId);
        post.comments.push(comment);

        // Display the updated posts
        displayPosts();

        // Clear the comment input
        commentInput.value = '';
    }

    function editPost(postId) {
        const post = posts.find(post => post.id === postId);

        // Get the current post details
        const currentTitle = post.title;
        const currentContent = post.content;

        // Prompt the user to edit the post
        const newTitle = prompt('Muokkaa otsikkoa:', currentTitle);
        const newContent = prompt('Muokkaa sisältöä:', currentContent);

        // Update the post with the new details
        if (newTitle !== null || newContent !== null) {
            post.title = newTitle || currentTitle;
            post.content = newContent || currentContent;

            // Display the updated posts
            displayPosts();
        }
    }

    function updatePostCounter() {
        postCounter.textContent = posts.length;
    }

    newPostLink.addEventListener('click', function () {
        newPostForm.style.display = 'block';
    });

    publishButton.addEventListener('click', function () {
        addPost();
    });

    function addPost() {
        const titleInput = document.getElementById('title');
        const contentInput = document.getElementById('content');

        // Check if title and content are not empty
        if (titleInput.value.trim() === '' || contentInput.value.trim() === '') {
            alert('Otsikko ja tekstiä tarvitaan.');
            return;
        }

        const title = titleInput.value;
        const content = contentInput.value;

        // Add the new post to the list
        const newPost = { id: posts.length + 1, title, content, comments: [] };
        posts.push(newPost);

        // Display the updated posts
        displayPosts();

        updatePostCounter(); // Update the post counter

        // Reset the form
        titleInput.value = '';
        contentInput.value = '';

        // Hide the form
        newPostForm.style.display = 'none';
    }

  function updatePostCounter() {
      postCounter.textContent = posts.length;
  }

    newPostLink.addEventListener('click', function () {
        newPostForm.style.display = 'block';
    });




});


  //proofiilii tallentaminen
  function kortin_arvot(kayttaja, mtiedot) {
    document.getElementById("kayttaja").innerHTML = kayttaja;
    document.getElementById("mtiedot").innerHTML = mtiedot;
 
  
    }
    function parametric() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const kayttaja = urlParams.get('kayttaja')
    const mtiedot = urlParams.get('mtiedot')
  

    
    kortin_arvot(kayttaja, mtiedot);
    }
    
    window.onload = parametric;
    
    

    

var loadFile = function (event) {
    var image = document.getElementById("profile-pic");
    image.src = URL.createObjectURL(event.target.files[0]);
};

document.getElementById("change-picture-btn").addEventListener("click", function () {
    document.getElementsByClassName("file").click();
});
